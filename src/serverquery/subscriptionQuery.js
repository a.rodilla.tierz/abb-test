
import { gql } from 'apollo-boost';

// Declaration of query for subscription to changeFeature
const querySubscriptionFeatures = gql`
                                      subscription{
                                        changeFeature {
                                          id,
                                          name,
                                          status,
                                          configurationFeature {
                                            nameFeature,
                                            numberComponents
                                          },
                                          components {
                                            compX {
                                              dimension,
                                              dev,
                                              devOut,
                                              validation
                                            },
                                            compY {
                                              dimension,
                                              dev,
                                              devOut,
                                              validation
                                            },
                                            compZ {
                                              dimension,
                                              dev,
                                              devOut,
                                              validation
                                            },
                                            compDiameter {
                                              dimension,
                                              dev,
                                              devOut,
                                              validation
                                            }
                                          }
                                        }
                                      }
                                      `;

export default querySubscriptionFeatures;
