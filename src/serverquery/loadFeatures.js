
import { gql } from 'apollo-boost';

// declaration of query for get the list of features wirt getFeaturesAutoUp
export const queryBaseFeatures = gql`
                    query queryInitial {
                      getFeaturesAutoUp {
                        id,
                        name,
                        status,
                        configurationFeature {
                          nameFeature,
                          numberComponents
                        },
                        components {
                          compX {
                            dimension,
                            dev,
                            devOut,
                            validation
                          },
                          compY {
                            dimension,
                            dev,
                            devOut,
                            validation
                          },
                          compZ {
                            dimension,
                            dev,
                            devOut,
                            validation
                          },
                          compDiameter {
                            dimension,
                            dev,
                            devOut,
                            validation
                          }
                        }
                      }
                    }
                  `;

export default queryBaseFeatures;
