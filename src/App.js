import React from 'react';
// import ReactDOM from "react-dom";
import './App.css';

import FeatureDataOutput from './components/featureDataOutput';

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <div className="App-intro">
          <FeatureDataOutput />
        </div>
      </div>
    );
  }
}

export default App;
