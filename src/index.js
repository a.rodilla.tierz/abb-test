import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { ApolloProvider } from '@apollo/react-hooks';
import { InMemoryCache } from 'apollo-cache-inmemory';
import {SubscriptionClient} from 'subscriptions-transport-ws';
import { HttpLink } from 'apollo-link-http';
import ApolloClient from 'apollo-client';
import { split } from 'apollo-link'
// import { getMainDefinition } from 'apollo-utilities'
import { WebSocketLink } from '@apollo/client/link/ws';

// declaration of addres to the server for access with the websockets
const wsClient = new SubscriptionClient(`ws://localhost:4000`, {
    reconnect: true,
    connectionParams: {
        // Pass any arguments you want for initialization
    }
});

const networkInterface = new HttpLink({
  uri: 'http://localhost:3000'
});

const wsLink = new WebSocketLink(wsClient);

// managin the queris on the websockets
const link = split(
  ({ query }) => {
    try {
      // const definition = getMainDefinition(query);
      return query
      // return (
      //   definition.kind === 'OperationDefinition' &&
      //   definition.operation === 'subscription'
      // );
    } catch(e) {
      console.error('error at: ', e);
      return query
    }
  },
  wsLink,
  networkInterface
)

const GraphQLClient = new ApolloClient({
  cache: new InMemoryCache(),
  link
});

// first render of the web
ReactDOM.render(
    <ApolloProvider client={GraphQLClient}>
      <App />
    </ApolloProvider>,
  document.getElementById('root')
);

serviceWorker.unregister();

export default GraphQLClient
