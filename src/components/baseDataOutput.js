import React from 'react';
import './baseDataOutput.css';

function BaseDataOutput(props) {
    return (
      <tr>
        <td>{props.params.dimension}</td>
        <td>{props.params.dev}</td>
        <td>{props.params.devOut}</td>
        <td>{props.params.validation}</td>
      </tr>
    );
}

export default BaseDataOutput;
