import React from 'react';
import BaseDataOutput from './baseDataOutput';
import './componentsDataOutput.css';

function ComponentsDataOutput(props) {
    return (
      <table className="ComponentTable">
        <BaseDataOutput params={props.params.compX} />
        <BaseDataOutput params={props.params.compY} />
        <BaseDataOutput params={props.params.compZ} />
        <BaseDataOutput params={props.params.compDiameter} />
      </table>
    );
}

export default ComponentsDataOutput;
