
import React from 'react';

import FeatureRowsOutput from './featureRowsOutput';
import { Query } from 'react-apollo';
import './featureDataOutput.css';

// Object with the query for the list of features
import queryBaseFeatures from './../serverquery/loadFeatures';
// Object with the query for declarate the subscription
import querySubscriptionFeatures from './../serverquery/subscriptionQuery';

// Manage the query to the server for take the information and send to draw
function FeatureDataOutput() {

    const DrawFeatures = () => {
      return (
        //  Call a query and manage the response for draw their infor and vinculate it to a subcripstion
        <Query query={queryBaseFeatures}>
          {({ loading, error, data, subscribeToMore}) => {
            if (loading) return <div>Loading...</div>;
            if (error) {
              console.error('error: ', error);
              return <div>Error!</div>;
            }
            _subscribeToNewLinks(subscribeToMore);
            return (
              data.getFeaturesAutoUp.map((itemFeature) => {
                return (
                    <table cellspacing="0" cellpadding="0" className="FeatureTable">
                        <tr className={itemFeature.status === 1 ? 'FeatureHeadOk' : itemFeature.status === 2 ? 'FeatureHeadWarning' : 'FeatureHeadFail'}>
                          <td className={itemFeature.components.length > 3 ? 'headDouble' : 'headSimple'}>{itemFeature.name}</td>
                        </tr>
                        {FeatureRowsOutput(itemFeature)}
                      </table>
                      )
              })
            )
          }}
        </Query>
    )};

    // Manage the subscription for the query, keep waiting for the response from the server, manage this information for integrate with the original datas and procesate it again
    const _subscribeToNewLinks = subscribeToMore => {
      subscribeToMore({
        document: querySubscriptionFeatures,
        updateQuery: (prev, { subscriptionData }) => {
          const newData = subscriptionData.data.changeFeature
          const indexUpdateFeature = prev.getFeaturesAutoUp.findIndex((nodeFeature) => {
            return newData.name === nodeFeature.name;
          });
          if (newData.components.length === (prev.getFeaturesAutoUp[indexUpdateFeature]['components']).length) {
            prev.getFeaturesAutoUp[indexUpdateFeature] = newData;
          }
          return prev;
        }
      })
    }

    return (
        <DrawFeatures />
    );
}

export default FeatureDataOutput;
