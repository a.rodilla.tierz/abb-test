import React from 'react';

import ComponentsDataOutput from './componentsDataOutput';
import './featureDataOutput.css';

const FeatureRowsOutput = (itemFeature) => {
    const secondListComponents = itemFeature.components.length > 3 ? itemFeature.components.splice((Math.trunc(itemFeature.components.length / 2))) : [];

    const defineHeadComponentBlock = (sizCheck) => {
      if (sizCheck > 0) {
          return (
                    <tr>
                      <td>
                        <table className="ComponentTable">
                          <tr>
                            <td>Control</td>
                            <td>Dev</td>
                            <td>Dev Out Tot</td>
                            <td></td>
                          </tr>
                        </table>
                      </td>
                      <td>
                        <table className="ComponentTable">
                          <tr>
                            <td>Control</td>
                            <td>Dev</td>
                            <td>Dev Out Tot</td>
                            <td></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  )
      }

      return (
                <tr>
                  <td>
                    <table className="ComponentTable">
                      <tr>
                        <td>Control</td>
                        <td>Dev</td>
                        <td>Dev Out Tot</td>
                        <td></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              )
    }

    return (
            <table cellspacing="0" cellpadding="0">
              {defineHeadComponentBlock(secondListComponents.length)}
              {itemFeature.components.map((nodeComFeature) => {
                if (secondListComponents.length > 0) {
                  return (
                            <tr>
                              <td><ComponentsDataOutput params={nodeComFeature} /></td>
                              <td><ComponentsDataOutput params={secondListComponents.shift()} /></td>
                            </tr>
                          )
                }
                return (
                          <tr>
                            <td><ComponentsDataOutput params={nodeComFeature} /></td>
                          </tr>
                        )
              })}
            </table>
            )
}

export default FeatureRowsOutput;
